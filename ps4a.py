# Problem Set 4A
# Name: Egor Kirnosov
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence:str):
    ans = []
    for i in range(0,len(sequence)):
        if len(sequence) == 1:
            return [sequence]
        symbol = sequence[i]
        subsequence = sequence[:i] + sequence[(i+1):]
        s_permutations = get_permutations(subsequence)
        for pm in s_permutations:
            new_str = symbol+pm
            if (not new_str in ans):
                ans.append(symbol+pm)
    return ans

if __name__ == '__main__':
     example_input = ['abc','sun','fun']
     expected_output = ["['abc', 'acb', 'bac', 'bca', 'cab', 'cba']","['sun', 'snu', 'usn', 'uns', 'nsu', 'nus']","['fun', 'fnu', 'ufn', 'unf', 'nfu', 'nuf']"]
     for i in range(0,3):
        print('Input:', example_input[i])
        print('Expected Output:', expected_output[i])
        print('Actual Output:', get_permutations(example_input[i]))


